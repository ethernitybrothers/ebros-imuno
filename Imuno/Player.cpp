#include "Player.h"
#include "Log.h"
#include "Engine.h"
#include "Body.h"
#include "BodyPart.h"
#include "PlayerData.h"

using libconfig::Setting;
using boost::format;

static Log logger;

Player::Player(int index)
{
	playerIndex = index;
    body.reset(new Body(this));
}


Player::~Player(void)
{
	for (std::list<BodyPart*>::iterator it = parts.begin(); it != parts.end(); it++) {
		delete *it;
	}
}

void Player::configure(const libconfig::Setting& setting)
{
	if (setting.exists("enabled"))
		enabled = setting["enabled"];
	else
		enabled = true;

	Log::indent block(logger);
	logger.info(format("enabled: %1%") % enabled);
	if (enabled) {
		if (setting.exists("body")) {
		    logger.info("body");
		    body->configure(setting["body"]);
		}
		if (setting.exists("parts")) {
			const Setting &setting_parts = setting["parts"];
		    int nelements = setting_parts.getLength();
		    for (int i = 0; i < nelements; i++) {
				logger.debug(format("part#%1%") % i);
				Setting &setting_part = setting_parts[i];
				BodyPart *body_part = new BodyPart(this);
				parts.push_back(body_part);
				body_part->configure(setting_part);
			}
		}
	}
}

void Player::init(Engine &engine)
{
	if (!enabled)
		return;
	body->init(engine);
	for (std::list<BodyPart*>::iterator it = parts.begin(); it != parts.end(); it++) {
		(*it)->init(engine);
	}
}

void Player::preRender(Engine &engine)
{
	if (!enabled)
		return;
	SceneData &sceneData = engine.getSceneData();
	if (!sceneData[playerIndex].active())
		return;

	body->preRender(engine);
	for (std::list<BodyPart*>::iterator it = parts.begin(); it != parts.end(); it++) {
		(*it)->preRender(engine);
	}
}

void Player::render(Engine &engine)
{
}

void Player::postRender(Engine &engine)
{
	if (!enabled)
		return;
	body->postRender(engine);
	for (std::list<BodyPart*>::iterator it = parts.begin(); it != parts.end(); it++) {
		(*it)->postRender(engine);
	}
}
