#pragma once
#include "BodyPartAnimation.h"

#include <memory>

class FloatFunction;

class BodyPartAnimationOpacityPulse : public BodyPartAnimation
{
public:
	BodyPartAnimationOpacityPulse(void);
	~BodyPartAnimationOpacityPulse(void);

	void animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data);

	void configure(const libconfig::Setting &setting);

private:
	float pulseFrequency;
	float opacityMultiplier;
	std::auto_ptr<FloatFunction> function;
};

