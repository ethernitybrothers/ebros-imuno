#include "Globals.h"

using libconfig::Setting;

Globals::Globals(void)
{
}

Globals::~Globals(void)
{
}

Globals Globals::instance;

void Globals::configure(const libconfig::Setting& setting)
{
	instance.scale = setting["scale"];
}
