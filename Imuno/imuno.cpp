#include "imuno.h"
#include "Log.h"
#include "Engine.h"
#include "Defaults.h"
#include "Globals.h"
#include "GLTexture.h"

#include <libconfig.hh>

using boost::format;
using std::string;
using libconfig::Setting;

static Log logger;

int imuno_main(int argc, const char **argv)
{

	logger.info("IMUNO Starting ...");
	libconfig::Config config;

	try {
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			logger.error("Failed to initialize SDL");
			return -1;
		}

		glewInit();

		Engine engine;
		const char* configFilename = "imuno.cfg";
		logger.info(format("Reading configuration file %1%") % configFilename);
		config.readFile(configFilename);

		const Setting& setting_defaults = config.lookup("defaults");
		Defaults::configure(setting_defaults);

		engine.configure(config);
		engine.init();
		engine.run();
		engine.finish();
	}
	catch (const libconfig::FileIOException &e) {
		logger.error("Configuration file imuno.cfg not found");
	}
	catch (const libconfig::SettingException &e) {
		const char *path = e.getPath();
		logger.error("Something wrong with configuration");
		if (path)
			logger.error(format("Path in the exception: %1%") % path);
	}
	catch (const libconfig::ParseException &e) {
		logger.error(format("Error in configuration at line %1%: %2%") % e.getLine() % e.getError());
	}
	catch (const std::exception& e) {
		logger.error(format("Exception: %1%") % e.what());
	}
	catch (...) {
		logger.error("Unknown exception");
	}

	logger.info("Finished");
	return 0;
}
