#pragma once

#include <libconfig.hh>

enum ColorMode;

class Defaults
{
private:
	Defaults(void);
	~Defaults(void);

	static Defaults instance;

	int zIndex;
	int bodyPartOffsetX;
	int bodyPartOffsetY;
	float pivotX;
	float pivotY;
	float scale;
	float rotation;
	float xFactor;
	float yFactor;
	float pulseFrequency;
	float scaleMultiplier;
	float opacity;
	float opacityMultiplier;
	float rotationSpeed;
	float colorR, colorG, colorB;
	float strength;
	unsigned maxHistory;
	float angleRange;
	float frequency;
	ColorMode colorMode;

public:
	static void configure(const libconfig::Setting &setting);

	static int getZindex() { return instance.zIndex; }

	static void getBodyPartOffset(int &offsetX, int &offsetY) {
		offsetX = instance.bodyPartOffsetX;
		offsetY = instance.bodyPartOffsetY;
	}

	static void getPivot(float &pivotX, float &pivotY)  {
		pivotX = instance.pivotX;
		pivotY = instance.pivotY;
	}

	static void getColor(float& colorR, float& colorG, float& colorB)  {
		colorR = instance.colorR;
		colorG = instance.colorG;
		colorB = instance.colorB;
	}

	static float getScale() {
		return instance.scale;
	}

	static float getRotation() {
		return instance.rotation;
	}

	static float getXFactor() {
		return instance.xFactor;
	}

	static float getYFactor() {
		return instance.yFactor;
	}

	static float getRotationSpeed() {
		return instance.rotationSpeed;
	}

	static float getPulseFrequency() {
		return instance.pulseFrequency;
	}

	static float getScaleMultiplier() {
		return instance.scaleMultiplier;
	}

	static float getOpacityMultiplier() {
		return instance.opacityMultiplier;
	}

	static float getOpacity() {
		return instance.opacity;
	}

	static float getStrength() {
		return instance.strength;
	}

	static unsigned getMaxHistory() {
		return instance.maxHistory;
	}

	static float getFrequency() {
		return instance.frequency;
	}

	static float getAngleRange() {
		return instance.angleRange;
	}

	static ColorMode getColorMode() {
		return instance.colorMode;
	}

};

