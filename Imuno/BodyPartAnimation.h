#pragma once
#include <libconfig.hh>

#include "PlayerData.h"

class Engine;

struct BodyPartAnimationData
{
	float x;
	float y;
	float w;
	float h;
	int offsetX1;
	int offsetY1;
	int offsetX2;
	int offsetY2;
	float pivotX;
	float pivotY;
	float rotation;
	float scale;
	float scaleX;
	float scaleY;
	float opacity;
	unsigned ticks;
	float colorR, colorG, colorB;
};

class BodyPartAnimation
{
public:
	virtual ~BodyPartAnimation() { }

	virtual void animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data) =0;

	virtual void configure(const libconfig::Setting &setting) { }
};