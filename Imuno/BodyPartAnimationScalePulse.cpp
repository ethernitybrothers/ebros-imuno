#include "BodyPartAnimationScalePulse.h"
#include "Defaults.h"
#include "FloatFunctionFactory.h"

#include <SDL.h>
#include <boost/math/constants/constants.hpp>
#include <cmath>

BodyPartAnimationScalePulse::BodyPartAnimationScalePulse(void)
{
	scaleMultiplier = Defaults::getScaleMultiplier();
	pulseFrequency = Defaults::getPulseFrequency();
}

BodyPartAnimationScalePulse::~BodyPartAnimationScalePulse(void)
{
}

void BodyPartAnimationScalePulse::configure(const libconfig::Setting &setting)
{
	if (setting.exists("scaleMultiplier")) {
		scaleMultiplier = setting["scaleMultiplier"];
	}
	if (setting.exists("pulseFrequency")) {
		pulseFrequency = setting["pulseFrequency"];
	}
	function.reset(FloatFunctionFactory::create("sin0_1"));
}

void BodyPartAnimationScalePulse::animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data)
{
	static float pi = boost::math::constants::pi<float>();
	float phase = (float) data.ticks * 2 * pi * pulseFrequency / 1000.0f;
	data.scale *=  1.0 + (scaleMultiplier-1.0) * (*function)(phase);
}



