#include "SceneLayer.h"
#include "Log.h"
#include "ImunoException.h"
#include "Defaults.h"
#include "Engine.h"
#include "PlayerSource.h"
#include "Defaults.h"

using libconfig::Setting;
using std::string;
using boost::format;

static Log logger;

SceneLayer::SceneLayer(void)
{
	zIndex = Defaults::getZindex();
	opacity = Defaults::getOpacity();
}

SceneLayer::~SceneLayer(void)
{
}

void SceneLayer::configure(const libconfig::Setting& setting)
{
	Log::indent block(logger);
	if (!setting.exists("img")) {
		throw ImunoException("Missing 'img' element in scene layer definition");
	}
	if (setting.exists("z-index")) {
		zIndex = (int) setting["z-index"];
	}
	if (setting.exists("opacity")) {
		opacity = (float) setting["opacity"];
	}

	const Setting &setting_img = setting["img"];
	filename = (const char *) setting_img;
	logger.debug(format("img: %1%") % filename);
	logger.debug(format("z-index: %1%") % zIndex);
	logger.debug(format("opacity: %1%") % opacity);
	texture.load(filename);
}

void SceneLayer::init(Engine &engine)
{
	texture.setup();
}

void SceneLayer::preRender(Engine &engine)
{
	engine.queue(this);
}

void SceneLayer::render(Engine &engine)
{
	int width, height;
	PlayerSource &playerSource = engine.getPlayerSource();
	playerSource.getResolution(width, height);
	glColor4f(1.0, 1.0, 1.0, opacity);
	texture.draw(0, 0, width, height, 1.0f, 1.0f, 1.0f, 1.0f, ColorModeModulate);
}

void SceneLayer::postRender(Engine &engine)
{
	texture.nextFrame();
}
