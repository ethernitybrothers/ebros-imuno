#pragma once
#include "BodyPartAnimation.h"

class BodyPartAnimationPendulum : public BodyPartAnimation
{
public:
	BodyPartAnimationPendulum(void);
	~BodyPartAnimationPendulum(void);

	void animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data);

	void configure(const libconfig::Setting &setting);

private:
	float angleRange;
	float frequency;
};

