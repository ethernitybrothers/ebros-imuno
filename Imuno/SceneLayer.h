#pragma once

#include <libconfig.hh>
#include <string>

#include "GLTexture.h"
#include "Layer.h"

class Engine;

class SceneLayer : public Layer
{
public:
	SceneLayer(void);
	~SceneLayer(void);
	void configure(const libconfig::Setting& setting);

	void init(Engine &engine);

	void preRender(Engine &engine);

	void render(Engine &engine);

	void postRender(Engine &engine);

	int getZindex() const { return zIndex; }

private:
	GLTexture texture;
	int zIndex;
	float opacity;
	std::string filename;
};

