#pragma once

#include <string>
#include <boost/format.hpp>

class Log
{
public:
	Log();
	~Log();

	void debug(const boost::format & fmt)
	{
		message("DEBUG:", fmt.str().c_str());
	}

	void debug(const char * msg)
	{
		message("DEBUG:", msg);
	}

	void info(const boost::format & fmt)
	{
		message("INFO: ", fmt.str().c_str());
	}

	void info(const char * msg)
	{
		message("INFO: ", msg);
	}

	void error(const boost::format & fmt)
	{
		message("ERROR:", fmt.str().c_str());
	}

	void error(const char * msg)
	{
		message("ERROR:", msg);
	}

	void beginIndent();
	void endIndent();

	class indent {
	public:
		indent(Log &_logger) : logger(_logger) { logger.beginIndent(); }
		~indent() { logger.endIndent(); }
	private:
		Log &logger;
	};

private:
	std::string domain;

	void message(const std::string& loglevel, const char * msg);

};

