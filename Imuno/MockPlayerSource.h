#pragma once

#include "PlayerSource.h"

#include <vector>
#include <boost/cstdint.hpp>

class MockPlayerSource : public PlayerSource
{
public:
	MockPlayerSource();
	~MockPlayerSource();

	void configure(const libconfig::Setting& setting);

	void getResolution(int &width, int &height) const {
		width = this->width;
		height = this->height;
	}

	int getMaxPlayers() const;

	void getFrame(std::vector<boost::uint16_t> &depthData, std::vector<boost::uint8_t> &playerData);

	void nextFrame();

private:
	std::vector<boost::uint8_t*> frames;
	int frameNo;
	int width;
	int height;
};

