#pragma once

#include <list>

class Engine;

class Layer
{
public:
	virtual ~Layer() { }

	virtual void render(Engine &engine) = 0;

	virtual int getZindex() const { return 0; };

	static bool z_index_sorter( const Layer *l1, const Layer *l2 ) {
		return l1->getZindex() < l2->getZindex();
	}
};