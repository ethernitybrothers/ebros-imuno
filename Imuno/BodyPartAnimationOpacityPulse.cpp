#include "BodyPartAnimationOpacityPulse.h"
#include "Defaults.h"
#include "FloatFunctionFactory.h"

#include <SDL.h>
#include <boost/math/constants/constants.hpp>
#include <cmath>

BodyPartAnimationOpacityPulse::BodyPartAnimationOpacityPulse(void)
{
	opacityMultiplier = Defaults::getOpacityMultiplier();
	pulseFrequency = Defaults::getPulseFrequency();
}

BodyPartAnimationOpacityPulse::~BodyPartAnimationOpacityPulse(void)
{
}

void BodyPartAnimationOpacityPulse::configure(const libconfig::Setting &setting)
{
	if (setting.exists("opacityMultiplier")) {
		opacityMultiplier = setting["opacityMultiplier"];
	}
	if (setting.exists("pulseFrequency")) {
		pulseFrequency = setting["pulseFrequency"];
	}
	function.reset(FloatFunctionFactory::create("sin0_1"));
}

void BodyPartAnimationOpacityPulse::animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data)
{
	static float pi = boost::math::constants::pi<float>();
	float phase = (float) data.ticks * 2 * pi * pulseFrequency / 1000.0f;
	data.opacity *=  1.0 + (opacityMultiplier-1.0) * (*function)(phase);
}

