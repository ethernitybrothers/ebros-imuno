#include "Defaults.h"
#include "GLTexture.h"

using libconfig::Setting;
using std::string;

Defaults::Defaults(void)
{
}

Defaults::~Defaults(void)
{
}

Defaults Defaults::instance;

void Defaults::configure(const libconfig::Setting& setting)
{
	const Setting &setting_pivot = setting["pivot"];
	instance.pivotX = setting_pivot[0];
	instance.pivotY = setting_pivot[1];

	const Setting &setting_bodyPart_offset = setting["bodyPart-offset"];
	instance.bodyPartOffsetX = setting_bodyPart_offset[0];
	instance.bodyPartOffsetY = setting_bodyPart_offset[1];

	const Setting &setting_color = setting["color"];
	instance.colorR = setting_color[0];
	instance.colorG = setting_color[1];
	instance.colorB = setting_color[2];

	instance.zIndex = setting["z-index"];

	instance.scale = setting["scale"];
	instance.rotation = setting["rotation"];
	instance.xFactor = setting["xFactor"];
	instance.yFactor = setting["yFactor"];
	instance.pulseFrequency = setting["pulseFrequency"];
	instance.scaleMultiplier = setting["scaleMultiplier"];
	instance.opacity = setting["opacity"];
	instance.opacityMultiplier = setting["opacityMultiplier"];
	instance.rotationSpeed = setting["rotationSpeed"];
	instance.maxHistory = setting["maxHistory"];
	instance.strength = setting["strength"];
	instance.frequency = setting["frequency"];
	instance.angleRange = setting["angleRange"];

	string colorMode = (const char*) setting["colorMode"];
	instance.colorMode = GLTexture::parseColorMode(colorMode);
}
