#include "BodyPartAnimationPendulum.h"
#include "Defaults.h"

#include <SDL.h>
#include <boost/math/constants/constants.hpp>


BodyPartAnimationPendulum::BodyPartAnimationPendulum(void)
{
	angleRange = Defaults::getAngleRange();
	frequency = Defaults::getFrequency();
}


BodyPartAnimationPendulum::~BodyPartAnimationPendulum(void)
{
}

void BodyPartAnimationPendulum::configure(const libconfig::Setting &setting)
{
	if (setting.exists("angleRange")) {
		angleRange = setting["angleRange"];
	}
	if (setting.exists("frequency")) {
		frequency = setting["frequency"];
	}
}

void BodyPartAnimationPendulum::animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data)
{
	static float pi = boost::math::constants::pi<float>();
	float phase = (float) data.ticks * 2 * pi * frequency / 1000.0f;
	data.rotation +=  angleRange * sin(phase);
}

