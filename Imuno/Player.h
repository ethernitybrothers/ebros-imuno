#pragma once

#include <libconfig.hh>

#include "Layer.h"

class Engine;
class BodyPart;
class Body;

class Player : public Layer
{
public:
	Player(int index);
	~Player(void);

	void configure(const libconfig::Setting& setting);

	void init(Engine &engine);

	void preRender(Engine &engine);

	void render(Engine &engine);

	void postRender(Engine &engine);

	int getIndex() const {
		return playerIndex;
	}

private:
	int playerIndex;
	bool enabled;
	std::auto_ptr<Body> body;
	std::list<BodyPart*> parts;
};

