#include "Log.h"

static int indentSpaces;

Log::Log()
{
}


Log::~Log(void)
{
}

void Log::message(const std::string &loglevel, const char * msg)
{
	printf("%s ", loglevel.c_str());
	for (int i = 0; i < indentSpaces; i++)
		printf(" ");
	printf("%s\n", msg);
}

void Log::beginIndent()
{
	indentSpaces += 4;
}

void Log::endIndent()
{
	indentSpaces -= 4;
}

