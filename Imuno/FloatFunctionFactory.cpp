#include "FloatFunctionFactory.h"
#include "LinearFunction.h"
#include "SinFunction.h"
#include "HyberbolicFunction.h"

#include "ImunoException.h"

#include <boost/math/constants/constants.hpp>

using boost::format;

FloatFunction * FloatFunctionFactory::create(const std::string &name)
{
	static float pi = boost::math::constants::pi<float>();

	if (name == "linear")
		return new LinearFunction();

	if (name == "sin")
		return new SinFunction();

	if (name == "cos") {
		SinFunction *cosine = new SinFunction();
		cosine->phase = pi / 2;
		return cosine;
	}

	if (name == "sin0_1") {
		SinFunction *sine = new SinFunction();
		sine->amplitude = 0.5;
		sine->offset = 0.5;
		return sine;
	}

	if (name == "hyperbolic0_1") {
		HyberbolicFunction *hyp = new HyberbolicFunction();
		hyp->offset = 1.0;
		hyp->scale = -1.0;
		hyp->phase = 1.0;
		return hyp;
	}

	throw ImunoException(format("Unknown float function \"%1%\"") % name);
}
