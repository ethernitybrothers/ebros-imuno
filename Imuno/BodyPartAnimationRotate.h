#pragma once
#include "BodyPartAnimation.h"

class BodyPartAnimationRotate : public BodyPartAnimation
{
public:
	BodyPartAnimationRotate(void);
	~BodyPartAnimationRotate(void);

	void animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data);

	void configure(const libconfig::Setting &setting);


private:
	float rotationSpeed;
};

