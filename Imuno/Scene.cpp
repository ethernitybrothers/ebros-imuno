#include "Scene.h"
#include "Log.h"
#include "Engine.h"
#include "SceneLayer.h"

static Log logger;

using libconfig::Setting;
using boost::format;

Scene::Scene()
{
}

Scene::~Scene()
{
	for (std::list<SceneLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		delete *layer;
	}

}

void Scene::configure(const libconfig::Setting& setting)
{
	Log::indent block(logger);

	const Setting &setting_background_layers = setting["layers"];

	int nelements = setting_background_layers.getLength();
	for (int i = 0; i < nelements; i++) {
		logger.debug(format("layer#%1%") % i);
		SceneLayer* layer = new SceneLayer();
		layer->configure(setting_background_layers[i]);
		layers.push_back(layer);
	}
}

void Scene::init(Engine &engine)
{
	for (std::list<SceneLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		(*layer)->init(engine);
	}
}

void Scene::preRender(Engine &engine)
{
	for (std::list<SceneLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		(*layer)->preRender(engine);
	}
}

void Scene::render(Engine &engine)
{
}

void Scene::postRender(Engine &engine)
{
	for (std::list<SceneLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		(*layer)->postRender(engine);
	}
}
