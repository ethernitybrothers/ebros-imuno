#pragma once
#include <list>
#include <string>
#include <libconfig.hh>

class BodyPartAnimation;

class BodyPartAnimationFactory
{
public:
	static std::list<BodyPartAnimation*> load(const libconfig::Setting &setting);

	static BodyPartAnimation * create(const std::string &type);

};

