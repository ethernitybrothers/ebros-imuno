#include "BodyPartAnimationRotateByCoords.h"
#include "Defaults.h"


BodyPartAnimationRotateByCoords::BodyPartAnimationRotateByCoords(void)
{
	xFactor = Defaults::getXFactor();
	yFactor = Defaults::getYFactor();
}

BodyPartAnimationRotateByCoords::~BodyPartAnimationRotateByCoords(void)
{
}

void BodyPartAnimationRotateByCoords::configure(const libconfig::Setting &setting)
{
	if (setting.exists("xFactor")) {
		xFactor = setting["xFactor"];
	}
	if (setting.exists("yFactor")) {
		yFactor = setting["yFactor"];
	}
}


void BodyPartAnimationRotateByCoords::animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data)
{
	data.rotation += data.x * xFactor + data.y * yFactor;
}

