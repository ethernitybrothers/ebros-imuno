#pragma once

#include <libconfig.hh>

class Globals
{
private:
	Globals(void);
	~Globals(void);

	static Globals instance;

	float scale;

public:
	static void configure(const libconfig::Setting &setting);

	static float getScale() { return instance.scale; }
};

