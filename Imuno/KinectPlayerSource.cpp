#include "KinectPlayerSource.h"
#include "Log.h"
#include "ImunoException.h"

#include <Windows.h>
#include <SDL.h>
#include <NuiApi.h>

struct KinectPlayerSource::Impl {
	// Current Kinect
    INuiSensor* nuiSensor;
	HANDLE depthStreamHandle;
    HANDLE nextDepthFrameEvent;

	SDL_Thread *thread;
	SDL_mutex *mutex;
	SDL_cond *cond;
};

static Log logger;

template<class Interface>
static void SafeRelease( Interface *& pInterfaceToRelease )
{
	if ( pInterfaceToRelease != NULL )
	{
		pInterfaceToRelease->Release();
		pInterfaceToRelease = NULL;
	}
}

KinectPlayerSource::KinectPlayerSource() : impl(new Impl)
{
    impl->nextDepthFrameEvent = INVALID_HANDLE_VALUE;
    impl->depthStreamHandle = INVALID_HANDLE_VALUE;
    impl->nuiSensor = NULL;
	impl->mutex = SDL_CreateMutex();
	impl->cond = SDL_CreateCond();
	running = false;
}

KinectPlayerSource::~KinectPlayerSource()
{
	SDL_DestroyMutex(impl->mutex);
	SDL_DestroyCond(impl->cond);
}

void KinectPlayerSource::getResolution(int &width, int &height) const
{
	width = this->width;
	height = this->height;
}

int KinectPlayerSource::getMaxPlayers() const {
	return 6;
}

void KinectPlayerSource::getFrame(std::vector<boost::uint16_t> &depthData, std::vector<boost::uint8_t> &playerData)
{
	SDL_mutexP(impl->mutex);
	depthData.resize(this->depthData.size());
	memcpy(depthData.data(), this->depthData.data(), sizeof(boost::uint16_t)*this->depthData.size());
	playerData.resize(this->playerData.size());
	memcpy(playerData.data(), this->playerData.data(), sizeof(boost::uint8_t)*this->playerData.size());
	SDL_mutexV(impl->mutex);
}

void KinectPlayerSource::nextFrame()
{
}

void KinectPlayerSource::init(Engine &engine)
{
	INuiSensor * pNuiSensor;
    HRESULT hr;

    int iSensorCount = 0;
    hr = NuiGetSensorCount(&iSensorCount);
    if (FAILED(hr))
    {
		throw ImunoException("No Nui Sensors found");
    }

    // Look at each Kinect sensor
    for (int i = 0; i < iSensorCount; ++i)
    {
        // Create the sensor so we can check status, if we can't create it, move on to the next
        hr = NuiCreateSensorByIndex(i, &pNuiSensor);
        if (FAILED(hr))
        {
            continue;
        }

        // Get the status of the sensor, and if connected, then we can initialize it
        hr = pNuiSensor->NuiStatus();
        if (S_OK == hr)
        {
            impl->nuiSensor = pNuiSensor;
            break;
        }

        // This sensor wasn't OK, so release it since we're not using it
        pNuiSensor->Release();
    }

    if (NULL != impl->nuiSensor)
    {
        // Initialize the Kinect and specify that we'll be using depth
        hr = impl->nuiSensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX); 
        if (SUCCEEDED(hr))
        {
            // Create an event that will be signaled when depth data is available
            impl->nextDepthFrameEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

            // Open a depth image stream to receive depth frames
            hr = impl->nuiSensor->NuiImageStreamOpen(
						NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX,
						NUI_IMAGE_RESOLUTION_640x480,
						0,
						2,
						impl->nextDepthFrameEvent,
						&impl->depthStreamHandle);
			width = 640;
			height = 480;
        }
    }

    if (NULL == impl->nuiSensor || FAILED(hr))
    {
		throw ImunoException("No ready Kinect found");
    }
	playerData.resize(640*480);

	SDL_mutexP(impl->mutex);
	running = true;
	SDL_mutexV(impl->mutex);
	impl->thread = SDL_CreateThread(thread_func, this);
}

void KinectPlayerSource::finish()
{
	SDL_mutexP(impl->mutex);
	running = false;
	SDL_CondSignal(impl->cond);
	SDL_mutexV(impl->mutex);
	SDL_WaitThread(impl->thread,  NULL);

	impl->nuiSensor->NuiShutdown();

	if (impl->nextDepthFrameEvent != INVALID_HANDLE_VALUE)
    {
        CloseHandle(impl->nextDepthFrameEvent);
    }

	SafeRelease(impl->nuiSensor);
}


int KinectPlayerSource::thread_func(void *param)
{
	KinectPlayerSource *source = reinterpret_cast<KinectPlayerSource*>(param);
	source->worker();
	return 0;
}


void KinectPlayerSource::worker()
{
	const int eventCount = 1;
    HANDLE hEvents[eventCount];
	hEvents[0] = impl->nextDepthFrameEvent;

    // Main message loop
    while (running)
    {
        // Check to see if we have either a message (by passing in QS_ALLINPUT)
        // Or a Kinect event (hEvents)
        // Update() will check for Kinect events individually, in case more than one are signalled
        DWORD dwEvent = MsgWaitForMultipleObjects(eventCount, hEvents, FALSE, INFINITE, QS_ALLINPUT);

        // Check if this is an event we're waiting on and not a timeout or message
        if (WAIT_OBJECT_0 == dwEvent)
        {
			if ( WAIT_OBJECT_0 == WaitForSingleObject(impl->nextDepthFrameEvent, 0) )
			{
				processData();
			}
        }

		/*
        if (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
        {
        }
		*/
    }

}

void KinectPlayerSource::processData()
{
	HRESULT hr;
    NUI_IMAGE_FRAME imageFrame;

    // Attempt to get the depth frame
    hr = impl->nuiSensor->NuiImageStreamGetNextFrame(impl->depthStreamHandle, 0, &imageFrame);
    if (FAILED(hr))
    {
        return;
    }

    INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
    NUI_LOCKED_RECT LockedRect;

    // Lock the frame data so the Kinect knows not to modify it while we're reading it
    pTexture->LockRect(0, &LockedRect, NULL, 0);

	std::vector<boost::uint16_t> tmpDepthData;
	tmpDepthData.resize(width*height);
	std::vector<boost::uint8_t> tmpPlayerData;
	tmpPlayerData.resize(width*height);

    // Make sure we've received valid data
    if (LockedRect.Pitch != 0)
    {
        const USHORT * pBufferRun = (const USHORT *) LockedRect.pBits;
        for (int y = 0; y < height; y++)
        {
			const USHORT *rowData = pBufferRun + y*width;
			unsigned rowIndex = (height-y-1)*width;
			for (int x = 0; x < width; x++) {
				// discard the portion of the depth that contains only the player index

				USHORT b = rowData[x];
				tmpDepthData[rowIndex+x] = NuiDepthPixelToDepth(b);
				tmpPlayerData[rowIndex+x] = NuiDepthPixelToPlayerIndex(b);
			}
        }
    }

    // We're done with the texture so unlock it
    pTexture->UnlockRect(0);

	SDL_mutexP(impl->mutex);
	depthData.swap(tmpDepthData);
	playerData.swap(tmpPlayerData);
	SDL_CondSignal(impl->cond);
	SDL_mutexV(impl->mutex);

    // Release the frame
    impl->nuiSensor->NuiImageStreamReleaseFrame(impl->depthStreamHandle, &imageFrame);
}

