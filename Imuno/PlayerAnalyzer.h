#pragma once
#include <libconfig.hh>
#include <vector>
#include <boost/cstdint.hpp>

class Engine;
class SceneData;
class PlayerSource;

class PlayerAnalyzer
{
public:
	PlayerAnalyzer(void);
	~PlayerAnalyzer(void);

	void configure(const libconfig::Setting &setting);

	void init(Engine &engine);

	void analyze(SceneData &playerData);
};
