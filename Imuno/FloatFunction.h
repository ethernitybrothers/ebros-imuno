#pragma once

class FloatFunction
{
public:
	virtual ~FloatFunction() { }

	virtual float operator() ( float argument ) const = 0;
};
