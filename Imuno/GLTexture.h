#pragma once

#include <Windows.h>
#include <SDL.h>
#include <SDL_image.h>
#include <GL/glew.h>
#include <gl/glu.h>
#include <string>
#include <vector>

enum ColorMode {
	ColorModeModulate,
	ColorModeAdd,
	ColorModeSubtract
};

class GLTexture
{
public:
	GLTexture();

	~GLTexture();

	void load(const std::string& file);
	void setup();
	void draw(int x1, int y1, int x2, int y2, float r, float g, float b, float a, const ColorMode colorMode);

	int getWidth() const {
		return width[frame];
	}

	int getHeight() const {
		return height[frame];
	}

	void nextFrame();
	
	static ColorMode parseColorMode(const std::string &s);

	static std::string colorModeToString(ColorMode colorMode);

private:
	std::vector<GLuint> textureId;
	std::vector<SDL_Surface*> surface;
	std::vector<int> width;
	std::vector<int> height;
	int frame;
	int nFrames;
};

