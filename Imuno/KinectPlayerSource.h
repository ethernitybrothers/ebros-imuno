#pragma once

#include "PlayerSource.h"

#include <memory>
#include <vector>

class KinectPlayerSource : public PlayerSource
{
public:
	KinectPlayerSource();
	~KinectPlayerSource();

	void init(Engine &engine);

	void finish();

	void getResolution(int &width, int &height) const;

	int getMaxPlayers() const;

	void getFrame(std::vector<boost::uint16_t> &depthData, std::vector<boost::uint8_t> &playerData);

	void nextFrame();

private:
	struct Impl;
	std::auto_ptr<Impl> impl;

	int width;
	int height;
	bool running;
	std::vector<boost::uint16_t> depthData;
	std::vector<boost::uint8_t> playerData;

	void worker();
	void processData();

	static int thread_func(void *param);

};

