#pragma once

#include <libconfig.hh>
#include <list>

#include "Layer.h"

class SceneLayer;
class Engine;

class Scene : public Layer
{
public:
	Scene();
	~Scene();

	void configure(const libconfig::Setting& setting);

	void init(Engine &engine);

	void preRender(Engine &engine);

	void render(Engine &engine);

	void postRender(Engine &engine);

private:
	std::list<SceneLayer*> layers;

};

