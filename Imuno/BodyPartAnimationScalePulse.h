#pragma once
#include "BodyPartAnimation.h"

#include <memory>

class FloatFunction;

class BodyPartAnimationScalePulse : public BodyPartAnimation
{
public:
	BodyPartAnimationScalePulse(void);
	~BodyPartAnimationScalePulse(void);

	void animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data);

	void configure(const libconfig::Setting &setting);

private:
	float pulseFrequency;
	float scaleMultiplier;
	std::auto_ptr<FloatFunction> function;
};

