#pragma once

#include <libconfig.hh>
#include <string>
#include <list>

#include "GLTexture.h"
#include "Layer.h"

class Engine;
class BodyPart;
class BodyPartAnimation;

class BodyPartLayer : public Layer
{
public:
	BodyPartLayer(BodyPart *bodyPart);
	~BodyPartLayer(void);

	void configure(const libconfig::Setting& setting);

	void init(Engine &engine);

	void preRender(Engine &engine);

	void render(Engine &engine);

	void postRender(Engine &engine);

	int getZindex() const { return zIndex; }

private:
	BodyPart *bodyPart;
	GLTexture texture;
	ColorMode colorMode;
	float opacity;
	float scale;
	float scaleX;
	float scaleY;
	float rotation;
	float pivotX;
	float pivotY;
	float colorR, colorG, colorB;
	int offsetX1;
	int offsetY1;
	int offsetX2;
	int offsetY2;
	int zIndex;
	std::string filename;
	std::list<BodyPartAnimation*> animations;
};

