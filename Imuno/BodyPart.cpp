#include "BodyPart.h"
#include "Log.h"
#include "Engine.h"
#include "Defaults.h"
#include "Player.h"
#include "BodyPartLayer.h"
#include "ImunoException.h"
#include "PlayerData.h"

#include <cassert>
#include <algorithm>

using libconfig::Setting;
using std::string;
using boost::format;

static Log logger;

static std::map<std::string, BodyPartType> createBodyPartMap()
{
	std::map<std::string, BodyPartType> bpm;
	bpm["top"] = BodyPartTop;
	bpm["left"] = BodyPartLeft;
	bpm["right"] = BodyPartRight;
	bpm["bottom"] = BodyPartBottom;
	bpm["center"] = BodyPartCenter;
	bpm["frame"] = BodyPartFrame;
	bpm["line"] = BodyPartLine;
	return bpm;
}

static std::map<std::string, PlayerPoint> createPlayerPointMap()
{
	std::map<std::string, PlayerPoint> bpm;
	bpm["top"] = PlayerPointTop;
	bpm["left"] = PlayerPointLeft;
	bpm["right"] = PlayerPointRight;
	bpm["bottom"] = PlayerPointBottom;
	bpm["center"] = PlayerPointCenter;
	bpm["bottomLeft"] = PlayerPointBottomLeft;
	bpm["bottomRight"] = PlayerPointBottomRight;
	bpm["topLeft"] = PlayerPointTopLeft;
	bpm["topRight"] = PlayerPointTopRight;
	return bpm;
}

std::map<std::string, BodyPartType> BodyPart::bodyPartTypeMap = createBodyPartMap();
std::map<std::string, PlayerPoint> BodyPart::playerPointMap = createPlayerPointMap();

BodyPart::BodyPart(Player *player)
{
	this->player = player;
	this->offsetX1 = 0;
	this->offsetY1 = 0;
	this->offsetX2 = 0;
	this->offsetY2 = 0;
	this->enabled = false;
}

BodyPart::~BodyPart(void)
{
	for (std::list<BodyPartLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		delete *layer;
	}
}

void BodyPart::configure(const libconfig::Setting& setting)
{
	Log::indent block(logger);

	if (setting.exists("enabled"))
		enabled = (bool) setting["enabled"];
	else
		enabled = true;

	logger.debug(format("enabled: %1%") % enabled);
	if (enabled) {

		string t = (const char *)  setting["type"];
		if (bodyPartTypeMap.find(t) == bodyPartTypeMap.end())
			throw ImunoException(format("Unknown body part %1%") % t);
		type = bodyPartTypeMap[t];
		if (type == BodyPartLine) {
			const Setting &setting_points = setting["points"];
			string p1 = (const char *)  setting_points[0];
			string p2 = (const char *)  setting_points[1];
			if (playerPointMap.find(p1) == playerPointMap.end() || playerPointMap.find(p2) == playerPointMap.end())
				throw ImunoException(format("Unknown line definition [\"%1%\", \"%2%\"]") % p1 % p2);
			lineFrom = playerPointMap[p1];
			lineTo = playerPointMap[p2];
		    logger.debug(format("type: %1% [%2%->%3%]") % t % p1 % p2);
		}
		else
		    logger.debug(format("type: %1% ") % t);

		if (setting.exists("offset")) {
			const Setting &setting_offset = setting["offset"];
			offsetX1 = (int) setting_offset[0];
			offsetY1 = (int) setting_offset[1];
			if (type == BodyPartLine) {
				if (setting_offset.getLength() < 4)
					throw ImunoException("Offset for type \"line\" must have 4 numbers");
				offsetX2 = (int) setting_offset[2];
				offsetY2 = (int) setting_offset[3];
			}
		}
		else {
			Defaults::getBodyPartOffset(offsetX1, offsetY1);
			Defaults::getBodyPartOffset(offsetX2, offsetY2);
		}
		if (type == BodyPartLine)
			logger.debug(format("offset: [%1%,%2%]->[%3%,%4%]") % offsetX1 % offsetY1 % offsetX2 % offsetY2);
		else
			logger.debug(format("offset: [%1%,%2%]") % offsetX1 % offsetY1);
	
		const Setting &setting_layers = setting["layers"];
		int nelements = setting_layers.getLength();
		for (int i = 0; i < nelements; i++) {
			logger.debug(format("layer#%1%") % i);
			Setting &setting_layer = setting_layers[i];
			BodyPartLayer* layer = new BodyPartLayer(this);
			layer->configure(setting_layer);
			layers.push_back(layer);
		}
	}
}

void BodyPart::init(Engine &engine)
{
	for (std::list<BodyPartLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		(*layer)->init(engine);
	}
}

void BodyPart::preRender(Engine &engine)
{
	if (!enabled)
		return;
	for (std::list<BodyPartLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		(*layer)->preRender(engine);
	}
}

void BodyPart::render(Engine &engine)
{
	if (!enabled)
		return;
}

void BodyPart::postRender(Engine &engine)
{
	if (!enabled)
		return;
	for (std::list<BodyPartLayer*>::iterator layer = layers.begin(); layer != layers.end(); layer++) {
		(*layer)->postRender(engine);
	}
}
