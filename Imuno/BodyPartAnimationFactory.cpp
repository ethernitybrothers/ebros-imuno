#include "BodyPartAnimationFactory.h"
#include "BodyPartAnimationRotateByCoords.h"
#include "BodyPartAnimationScalePulse.h"
#include "BodyPartAnimationScaleByMass.h"
#include "BodyPartAnimationOpacityPulse.h"
#include "BodyPartAnimationPendulum.h"
#include "BodyPartAnimationRotate.h"
#include "BodyPartAnimationInertia.h"
#include "BodyPartAnimationSmooth.h"

#include "ImunoException.h"

using std::list;
using std::string;
using boost::format;
using libconfig::Setting;

std::list<BodyPartAnimation*> BodyPartAnimationFactory::load(const libconfig::Setting &setting)
{
	list<BodyPartAnimation*> animations;
	if (setting.isList()) {
		for (int i = 0; i < setting.getLength(); i++) {
			const Setting& setting_animation = setting[i];
			string type = (const char *) setting_animation["type"];
			BodyPartAnimation * animation = create(type);
			animation->configure(setting_animation);
			animations.push_back(animation);
		}
	}
	else {
		string type = (const char *) setting["type"];
		BodyPartAnimation * animation = create(type);
		animation->configure(setting);
		animations.push_back(animation);
	}
	
	return animations;
}

BodyPartAnimation * BodyPartAnimationFactory::create(const std::string& type)
{
	if (type == "rotateByCoords") {
		return new BodyPartAnimationRotateByCoords();
	}
	else if (type == "scalePulse") {
		return new BodyPartAnimationScalePulse();
	}
	else if (type == "opacityPulse") {
		return new BodyPartAnimationOpacityPulse();
	}
	else if (type == "pendulum") {
		return new BodyPartAnimationPendulum();
	}
	else if (type == "rotate") {
		return new BodyPartAnimationRotate();
	}
	else if (type == "inertia") {
		return new BodyPartAnimationInertia();
	}
	else if (type == "smooth") {
		return new BodyPartAnimationSmooth();
	}
	else if (type == "scaleByMass") {
		return new BodyPartAnimationScaleByMass();
	}
	throw ImunoException(format("Unknown animation type \"%1\"") % type);
}
