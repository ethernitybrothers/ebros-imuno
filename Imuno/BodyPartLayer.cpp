#include "BodyPartLayer.h"
#include "Log.h"
#include "Engine.h"
#include "Defaults.h"
#include "ImunoException.h"
#include "BodyPart.h"
#include "Player.h"
#include "PlayerData.h"
#include "BodyPartAnimation.h"
#include "BodyPartAnimationFactory.h"

#include <string>
#include <cmath>
#include <boost/math/constants/constants.hpp>

using libconfig::Setting;
using boost::format;
using std::string;
using std::list;

static Log logger;

BodyPartLayer::BodyPartLayer(BodyPart *bodyPart)
{
	this->bodyPart = bodyPart;
	zIndex = Defaults::getZindex();
	opacity = Defaults::getOpacity();
	Defaults::getPivot(pivotX, pivotY);
	scale = Defaults::getScale();
	rotation = Defaults::getRotation();
	offsetX1 = 0;
	offsetY1 = 0;
	offsetX2 = 0;
	offsetY2 = 0;
	scaleX = 1;
	scaleY = 1;
	colorMode = Defaults::getColorMode();
	Defaults::getColor(colorR, colorG, colorB);
}

BodyPartLayer::~BodyPartLayer(void)
{
	for (list<BodyPartAnimation*>::iterator it = animations.begin(); it != animations.end(); it++) {
		delete *it;
	}
}

void BodyPartLayer::configure(const libconfig::Setting& setting)
{
	Log::indent block(logger);
	if (!setting.exists("img")) {
		throw ImunoException("Missing 'img' element in bodyPart layer definition");
	}
	if (setting.exists("pivot")) {
		const Setting &setting_pivot = setting["pivot"];
		pivotX = setting_pivot[0];
		pivotY = setting_pivot[1];
	}

	if (setting.exists("z-index")) {
		zIndex = setting["z-index"];
	}

	if (setting.exists("opacity")) {
		opacity = setting["opacity"];
	}

	if (setting.exists("scale")) {
		scale = setting["scale"];
	}

	if (setting.exists("scaleX")) {
		scaleX = setting["scaleX"];
	}

	if (setting.exists("scaleY")) {
		scaleY = setting["scaleY"];
	}

	if (setting.exists("rotation")) {
		rotation = setting["rotation"];
	}
	
	if (setting.exists("animation")) {
		const Setting &setting_animation = setting["animation"];
		animations = BodyPartAnimationFactory::load(setting_animation);
	}

	if (setting.exists("offset")) {
		const Setting &setting_offset = setting["offset"];
		offsetX1 = setting_offset[0];
		offsetY1 = setting_offset[1];
		if (bodyPart->getType() == BodyPartLine) {
			if (setting_offset.getLength() < 4)
				throw ImunoException("Offset for type \"line\" must have 4 numbers");
			offsetX2 = (int) setting_offset[2];
			offsetY2 = (int) setting_offset[3];
		}
	}

	if (setting.exists("color")) {
		const Setting &setting_color = setting["color"];
		colorR = setting_color[0];
		colorG = setting_color[1];
		colorB = setting_color[2];
	}

	if (setting.exists("colorMode")) {
		string s = (const char *) setting["colorMode"];
		colorMode = GLTexture::parseColorMode(s);
	}

	filename = (const char *) setting["img"];
	logger.debug(format("file: %1%") % filename);
	logger.debug(format("color: [%1%,%2%,%3%]") % colorR % colorG % colorB);
	if (bodyPart->getType() == BodyPartLine)
		logger.debug(format("offset: [%1%,%2%]->[%3%,%4%]") % offsetX1 % offsetY1 % offsetX2 % offsetY2);
	else
		logger.debug(format("offset: [%1%,%2%]") % offsetX1 % offsetY1);

	logger.debug(format("pivot: [%1%,%2%]") % pivotX % pivotY);
	logger.debug(format("z-index: %1%") % zIndex);
	logger.debug(format("opacity: %1%") % opacity);
	logger.debug(format("scale: %1% x [%2%, %3%]") % scale % scaleX % scaleY);
	logger.debug(format("colorMode: %1%") % GLTexture::colorModeToString(colorMode));
	texture.load(filename);
}

void BodyPartLayer::init(Engine &engine)
{
	texture.setup();
}

void BodyPartLayer::preRender(Engine &engine)
{
	engine.queue(this);
}

void BodyPartLayer::render(Engine &engine)
{
	static float pi = boost::math::constants::pi<float>();

	int playerIndex = bodyPart->getPlayer()->getIndex();
	SceneData &sceneData = engine.getSceneData();
	PlayerData &playerData = sceneData[playerIndex];

	BodyPartAnimationData bodyPartData;
	bodyPart->getOffset(bodyPartData.offsetX1, bodyPartData.offsetY1, bodyPartData.offsetX2, bodyPartData.offsetY2);
	bodyPartData.offsetX1 += offsetX1;
	bodyPartData.offsetY1 += offsetY1;
	bodyPartData.offsetX2 += offsetX2;
	bodyPartData.offsetY2 += offsetY2;
	bodyPartData.pivotX = pivotX;
	bodyPartData.pivotY = pivotY;
	bodyPartData.rotation = rotation;
	bodyPartData.scale = scale;
	bodyPartData.scaleX = scaleX;
	bodyPartData.scaleY = scaleY;
	bodyPartData.opacity = opacity;
	bodyPartData.ticks = engine.getTicks();
	bodyPartData.colorR = colorR;
	bodyPartData.colorG = colorG;
	bodyPartData.colorB = colorB;

	int p1x, p1y, p2x, p2y;
	PlayerPoint lineFrom, lineTo;

	switch (bodyPart->getType()) {
	case BodyPartTop:
		playerData.getPlayerPoint(PlayerPointTop, bodyPartData.x, bodyPartData.y);
		bodyPartData.w = texture.getWidth();
		bodyPartData.h = texture.getHeight();
		// logger.debug(format("head  : [%03d,%03d]") % (int) cx % (int) cy);
		break;
	case BodyPartLeft:
		playerData.getPlayerPoint(PlayerPointLeft, bodyPartData.x, bodyPartData.y);
		bodyPartData.w = texture.getWidth();
		bodyPartData.h = texture.getHeight();
		// logger.debug(format("left  : [%03d,%03d]") % (int) cx % (int) cy);
		break;
	case BodyPartRight:
		playerData.getPlayerPoint(PlayerPointRight, bodyPartData.x, bodyPartData.y);
		bodyPartData.w = texture.getWidth();
		bodyPartData.h = texture.getHeight();
		// logger.debug(format("right : [%03d,%03d]") % (int) cx % (int) cy);
		break;
	case BodyPartBottom:
		playerData.getPlayerPoint(PlayerPointBottom, bodyPartData.x, bodyPartData.y);
		bodyPartData.w = texture.getWidth();
		bodyPartData.h = texture.getHeight();
		// logger.debug(format("right : [%03d,%03d]") % (int) cx % (int) cy);
		break;
	case BodyPartCenter:
		playerData.getPlayerPoint(PlayerPointCenter, bodyPartData.x, bodyPartData.y);
		bodyPartData.w = texture.getWidth();
		bodyPartData.h = texture.getHeight();
		// logger.debug(format("center: [%03d,%03d]") % (int) cx % (int) cy);
		break;
	case BodyPartFrame:
		bodyPartData.x = (playerData.leftmost_x + playerData.rightmost_x) / 2;
		bodyPartData.y = (playerData.topmost_y + playerData.lowermost_y) / 2;
		bodyPartData.w = playerData.rightmost_x - playerData.leftmost_x;
		bodyPartData.h = playerData.topmost_y - playerData.lowermost_y;
		break;
	case BodyPartLine:
		bodyPart->getLinePoints(lineFrom, lineTo);
		playerData.getPlayerPoint(lineFrom, p1x, p1y);
		playerData.getPlayerPoint(lineTo, p2x, p2y);
		p1x += bodyPartData.offsetX1;
		p1y += bodyPartData.offsetY1;
		p2x += bodyPartData.offsetX2;
		p2y += bodyPartData.offsetY2;
		bodyPartData.x = (p1x + p2x) / 2;
		bodyPartData.y = (p1y + p2y) / 2;
		bodyPartData.rotation = 180.0 * atan2((double) (p2x-p1x), (double) (p1y-p2y)) / pi - 90.0;
		bodyPartData.w = sqrt(pow((double) (p1x-p2x), 2.0) + pow((double) (p1y-p2y), 2.0));
		bodyPartData.h = texture.getHeight();
		break;
	}

	// now run the animation algorithms
	for (list<BodyPartAnimation*>::iterator it = animations.begin(); it != animations.end(); it++) {
		BodyPartAnimation *animation = *it;
		animation->animate(engine, playerData, bodyPartData);
	} 

	glPushMatrix();
	if (bodyPart->getType() == BodyPartLine)
		glTranslatef(bodyPartData.x, bodyPartData.y, 0);
	else
		glTranslatef(bodyPartData.x+bodyPartData.offsetX1, bodyPartData.y+bodyPartData.offsetY1, 0);

	glRotatef(bodyPartData.rotation, 0.0f, 0.0f, 1.0f);
	glScalef(bodyPartData.scale * bodyPartData.scaleX, bodyPartData.scale * bodyPartData.scaleY, 0.0f);

	float px = ((float) bodyPartData.w * bodyPartData.pivotX);
	float py = ((float) bodyPartData.h * (1-bodyPartData.pivotY));
	glTranslatef(-px, -py, 0);

	texture.draw(0, 0, bodyPartData.w, bodyPartData.h, bodyPartData.colorR, bodyPartData.colorG, bodyPartData.colorB, bodyPartData.opacity, colorMode); 

	glPopMatrix();
}

void BodyPartLayer::postRender(Engine &engine)
{
	texture.nextFrame();
}