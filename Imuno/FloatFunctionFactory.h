#pragma once

#include <string>
#include "FloatFunction.h"

class FloatFunctionFactory
{
public:
	static FloatFunction * create(const std::string &name);
};

