#include "MockPlayerSource.h"
#include "Log.h"
#include "ImunoException.h"

#include <SDL.h>
#include <SDL_image.h>

using libconfig::Setting;
using boost::format;
using std::string;

static Log logger;

MockPlayerSource::MockPlayerSource()
{
	frameNo = 0;
	width = 0;
	height = 0;
}


MockPlayerSource::~MockPlayerSource()
{
	for (std::vector<uint8_t*>::iterator plrdata = frames.begin(); plrdata != frames.end(); plrdata++) {
		delete[] *plrdata;
	}
}

inline static
Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void MockPlayerSource::configure(const libconfig::Setting& setting)
{
	Log::indent block(logger);
	const Setting &setting_image_sequence = setting["image_sequence"];
	string image_sequence = (const char *) setting_image_sequence;
	logger.debug(format("image_sequence: %1%") % image_sequence);
	int frame = 0;

	for (;;) {
		string oneFile = (format(image_sequence) % frame).str();
		SDL_Surface *surface = IMG_Load(oneFile.c_str());
		if (surface) {

			if (width == 0) {
				width = surface->w;
				height = surface->h;
			}
			else if (width != surface->w || height != surface->h) {
				SDL_FreeSurface(surface);
				throw ImunoException(format("Image %1% in sequence has different resolution than previous images") % oneFile);
			}

			logger.debug(format("frame#%1%: %2%") % frame % oneFile);
			uint8_t *plrdata = new uint8_t[surface->w * surface->h];

			for (int y = 0; y < surface->h; y++) {
				for (int x = 0; x < surface->w; x++) {
					Uint32 pixel = getpixel(surface, x, y);
					Uint8 r,g,b;
					SDL_GetRGB(pixel, surface->format, &r, &g, &b);


					// now guess the player by RGB values
					uint8_t player;
					if (r > 0x80 && g < 0x30 && b < 0x30)
						player = 1;
					else if (g > 0x80 && r < 0x30 && b < 0x30)
						player = 2;
					else if (b > 0x80 && r < 0x30 && g < 0x30)
						player = 3;
					else
						player = 0;
					plrdata[(surface->h - y - 1)*surface->w + x] = player;
				}
			}
	
			frames.push_back(plrdata);
			frame++;

			SDL_FreeSurface(surface);
		}
		else
			break;
	}
	if (frames.empty())
		throw ImunoException("Image sequence is empty");

}

int MockPlayerSource::getMaxPlayers() const {
	return 3;
}


void MockPlayerSource::getFrame(std::vector<boost::uint16_t> &depthData, std::vector<boost::uint8_t> &playerData)
{
	unsigned size = width*height;
	depthData.resize(size);
	memset(depthData.data(), 0, sizeof(boost::uint16_t)*size);
	playerData.resize(size);
	memcpy(playerData.data(), frames[frameNo], sizeof(boost::uint8_t)*size);
}

void MockPlayerSource::nextFrame() {
	frameNo++;
	if (frameNo >= frames.size())
		frameNo = 0;
}

