#include "BodyPartAnimationInertia.h"
#include "FloatFunctionFactory.h"
#include "Defaults.h"
#include <cmath>
#include <boost/math/constants/constants.hpp>

using std::deque;

BodyPartAnimationInertia::BodyPartAnimationInertia(void)
{
	strength = Defaults::getStrength();
	maxHistory = Defaults::getMaxHistory();
	strengthDamping = 0.9;
}

BodyPartAnimationInertia::~BodyPartAnimationInertia(void)
{
}

void BodyPartAnimationInertia::animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data)
{
	static float pi = boost::math::constants::pi<float>();
	coord_history.push_back(coord_t(data.x, data.y));
	if (coord_history.size() > maxHistory)
		coord_history.pop_front();

	float appliedStrength = strength;
	const coord_t & c2 = coord_history.back();
	for (deque<coord_t>::const_reverse_iterator it = coord_history.rbegin(); it != coord_history.rend(); it++) {
		if (it == coord_history.rbegin())
			continue; // discard the first

		const coord_t & c1 = (*it);
		float dx = c2.x - c1.x;
		float dy = c2.y - c1.y;
		float length = sqrt(dx*dx + dy*dy);
		float angle = 360 * atan2(dx, dy) / (2 * pi);

		float angleRange = angle - data.rotation;
		float intensity = (*function)(length * appliedStrength / 1000.0);
		data.rotation -= angleRange * intensity;
		appliedStrength *= strengthDamping;
	}
}

void BodyPartAnimationInertia::configure(const libconfig::Setting& setting)
{
	if (setting.exists("strength")) {
		strength = setting["strength"];
	}
	if (setting.exists("historyMax")) {
		maxHistory = setting["maxHistory"];
	}
	if (setting.exists("strengthDamping")) {
		strengthDamping = setting["strengthDamping"];
	}
	function.reset(FloatFunctionFactory::create("hyperbolic0_1"));
}


