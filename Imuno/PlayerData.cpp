#include "PlayerData.h"


SceneData::SceneData(void)
{
}


SceneData::~SceneData(void)
{
}

void SceneData::clear()
{
}

void SceneData::allocatePlayers(int nPlayers)
{
	if (players.size() < nPlayers+1) {
		players.resize(nPlayers+1, PlayerData());
	}
}

void PlayerData::getPlayerPoint(PlayerPoint playerPoint, int &x, int &y) const
{
	switch (playerPoint) {
	case PlayerPointBottom:
		x = topmost_x;
		y = topmost_y;
		break;
	case PlayerPointLeft:
		x = leftmost_x;
		y = leftmost_y;
		break;
	case PlayerPointRight:
		x = rightmost_x;
		y = rightmost_y;
		break;
	case PlayerPointTop:
		x = lowermost_x;
		y = lowermost_y;
		break;
	case PlayerPointCenter:
		x = center_x;
		y = center_y;
		break;
	case PlayerPointBottomLeft:
		x = leftmost_x;
		y = topmost_y;
		break;
	case PlayerPointBottomRight:
		x = rightmost_x;
		y = topmost_y;
		break;
	case PlayerPointTopLeft:
		x = leftmost_x;
		y = lowermost_y;
		break;
	case PlayerPointTopRight:
		x = rightmost_x;
		y = lowermost_y;
		break;
	}
}
