#include "Engine.h"
#include "Log.h"
#include "KinectPlayerSource.h"
#include "MockPlayerSource.h"
#include "ImunoException.h"
#include "PlayerData.h"
#include "PlayerSource.h"
#include "PlayerAnalyzer.h"
#include "Player.h"
#include "Scene.h"

#include <Windows.h>
#include <SDL.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <algorithm>

using boost::format;
using std::vector;
using std::sort;
using libconfig::Setting;

static Log logger;

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

Engine::Engine() :
	scene(new Scene()),
	playerAnalyzer(new PlayerAnalyzer()),
	sceneData(new SceneData())
{
	surface = 0;
}

Engine::~Engine(void)
{
	SDL_FreeSurface(surface);
	for (std::vector<Player*>::iterator player = players.begin(); player != players.end(); player++) {
		delete *player;
	}
}

void Engine::configure(const libconfig::Config& config)
{
	logger.info("--ENGINE CONFIG--");
	logger.info("Scene");
	const Setting& setting_scene = config.lookup("scene");
	scene->configure(setting_scene);

	// players
	const Setting& setting_players = config.lookup("players");
	int nplayers = setting_players.getLength();
	for (int playerIndex = 0; playerIndex < nplayers; playerIndex++) {
		const Setting& setting_player = setting_players[playerIndex];
		Player* player = new Player(playerIndex+1);
		logger.info(format("Player#%1%") % (playerIndex+1));
		player->configure(setting_player);
		players.push_back(player);
	}

	width = config.lookup("engine.resolution")[0];
	height = config.lookup("engine.resolution")[1];
	fullscreen = config.lookup("engine.fullscreen");
	antialiasing = config.lookup("engine.antialiasing");
	maxZindex = config.lookup("engine.max-z-index");
	maxFrameRate = config.lookup("engine.max-frame-rate");

	const Setting& setting_player_source = config.lookup("engine.player_source");
	std::string psource = (const char *) setting_player_source["driver"];
	if (psource == "mock") {
		playerSource.reset(new MockPlayerSource());
		logger.info("Player source Mock");
	}
	else if (psource == "kinect") {
		playerSource.reset(new KinectPlayerSource());
		logger.info("Player source Kinect");
	}
	else {
		throw ImunoException(format("Unknown player source \"%1%\"") % psource);
	}
	playerSource->configure(setting_player_source);
	const Setting& setting_player_analyzer = config.lookup("engine.player_analyzer");
	playerAnalyzer->configure(setting_player_analyzer);
}

void Engine::init()
{
	logger.info("--ENGINE INIT--");
	Uint32 flags;
	if (fullscreen)
		flags = SDL_OPENGL | SDL_FULLSCREEN;
	else 
		flags = SDL_OPENGL;

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	if (antialiasing) {
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
	}

	if ((surface = SDL_SetVideoMode(width, height, 32, flags)) == NULL) {
		throw ImunoException(format("Failed to set video mode %1%x%2%") % width % height);
	}
	logger.info("Checking OpenGL");
	const char *glvendor = (const char *) glGetString(GL_VENDOR);
	const char *glrenderer = (const char *) glGetString(GL_RENDERER);
	const char *glversion = (const char *) glGetString(GL_VERSION);
	logger.info(format("OpenGL vendor: %1%, renderer: %2%, version: %3%") % (glvendor ? glvendor : "<N/A>") % (glrenderer ? glrenderer : "<N/A>") % (glversion ? glversion : "<N/A>"));
 
	scene->init(*this);
	for (std::vector<Player*>::iterator player = players.begin(); player != players.end(); player++) {
		(*player)->init(*this);
	}
	playerSource->init(*this);

	playerSource->getResolution(pd_width, pd_height);

	if (antialiasing)
		glEnable(GL_MULTISAMPLE);
 
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, pd_width, pd_height, 0.0f, -1, 1);
 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	logger.info(format("Initialized video mode %1%x%2% %3%") % width % height % (fullscreen ? "fullscreen" : "windowed"));
}

void Engine::run()
{
	logger.info("--ENGINE RUN--");

	bool quit = false;
	SDL_Event event;
	SDL_ShowCursor(0);

	unsigned frameNo = 0;

	unsigned minMillis = 1000 / maxFrameRate;

	while ( quit == false ) {
		unsigned renderBegin = SDL_GetTicks();
		while ( SDL_PollEvent( &event ) ) {
			//If the user has Xed out the window
			if (event.type == SDL_QUIT) {
				//Quit the program
				quit = true;
			}
			else if (event.type == SDL_KEYDOWN) {

				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
				case SDLK_q:
					quit = true;
					break;
				case SDLK_r:
				case SDLK_PAGEUP:
				case SDLK_PAGEDOWN:
				case SDLK_1:
				case SDLK_2:
					break;
				}
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		sceneData->clear();
		playerSource->getResolution(sceneData->width, sceneData->height);
		sceneData->maxPlayers = playerSource->getMaxPlayers();
		playerSource->getFrame(sceneData->depthBuffer, sceneData->playerIndexBuffer);
		playerAnalyzer->analyze(*sceneData);
		ticks = SDL_GetTicks();
		render();
		SDL_GL_SwapBuffers();
		playerSource->nextFrame();
		unsigned renderEnd = SDL_GetTicks();
		unsigned timeConsumed = renderEnd - renderBegin;
		if (timeConsumed < minMillis) {
			SDL_Delay(minMillis - timeConsumed);
		}
		frameNo ++;
	}

}

void Engine::render()
{
	layerQueue.clear();
	scene->preRender(*this);
	for (std::vector<Player*>::iterator player = players.begin(); player != players.end(); player++) {
		(*player)->preRender(*this);
	}

	sort(layerQueue.begin(), layerQueue.end(), Layer::z_index_sorter);

	// draw from back
	for (vector<Layer*>::iterator layer = layerQueue.begin(); layer != layerQueue.end(); layer++) {
		(*layer)->render(*this);
	}

	scene->postRender(*this);
	for (std::vector<Player*>::iterator player = players.begin(); player != players.end(); player++) {
		(*player)->postRender(*this);
	}

}

void Engine::finish()
{
	playerSource->finish();
}
