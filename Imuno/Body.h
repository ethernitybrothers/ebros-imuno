#pragma once

#include <libconfig.hh>
#include <boost/cstdint.hpp>

#include "GLTexture.h"
#include "Layer.h"

class Engine;
class Player;

class Body : public Layer
{
public:
	Body(Player *player);
	~Body(void);

	void configure(const libconfig::Setting &setting);

	void init(Engine &engine);

	void preRender(Engine &engine);

	void render(Engine &engine);

	void postRender(Engine &engine);

	int getZindex() const { return zIndex; }

private:
	Player *player;
	GLTexture texture;
	float opacity;
	boost::uint8_t *stencilBuffer;
	int zIndex;
	unsigned *xindexes;
};

