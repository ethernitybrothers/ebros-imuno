#pragma once

#include <SDL.h>
#include <vector>
#include <libconfig.hh>

class PlayerAnalyzer;
class PlayerSource;
class SceneData;
class Scene;
class Player;
class Layer;

class Engine
{
public:
	Engine();
	~Engine(void);

	void configure(const libconfig::Config& config);

	void init();

	void run();

	void finish();

	void getResolution(int &width, int &height) const {
		width = this->width;
		height = this->height;
	}

	void queue(Layer *layer) {
		layerQueue.push_back(layer);
	}

	PlayerSource & getPlayerSource() {
		return *playerSource;
	}

	SceneData & getSceneData() {
		return *sceneData;
	}

	unsigned getTicks() const {
		return ticks;
	}

	void coordPlayerToScreen(int srcx, int srcy, int &dstx, int &dsty) const {
		dstx = (srcx * width) / pd_width;
		dsty = (srcy * height) / pd_height;
	}

private:
	SDL_Surface *surface;
	int width;
	int height;
	bool fullscreen;
	bool antialiasing;
	int maxZindex;
	int pd_width;
	int pd_height;
	unsigned maxFrameRate;
	unsigned ticks;
	std::vector<Player*> players;
	std::vector<Layer*> layerQueue;
	std::auto_ptr<Scene> scene;
	std::auto_ptr<PlayerAnalyzer> playerAnalyzer;
	std::auto_ptr<PlayerSource> playerSource;
	std::auto_ptr<SceneData> sceneData;

	void render();
};

