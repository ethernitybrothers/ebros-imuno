#pragma once

#include "FloatFunction.h"

#include <cmath>

class SinFunction : public FloatFunction
{
public:
	float operator() (float argument) const {
		return amplitude * sin(argument + phase) + offset;
	}

	SinFunction() {
		amplitude = 1.0;
		phase = 0.0;
		offset = 0.0;
	}

	float amplitude;
	float offset;
	float phase;
};
