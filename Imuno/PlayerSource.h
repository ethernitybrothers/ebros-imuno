#pragma once

#include <libconfig.hh>
#include <boost/cstdint.hpp>
#include <vector>

class Engine;

class PlayerSource
{
public:
	virtual ~PlayerSource() { }

	virtual void configure(const libconfig::Setting& setting) { }

	virtual void init(Engine &engine) { }

	virtual void finish() { }

	virtual void getResolution(int &width, int &height) const = 0;

	virtual int getMaxPlayers() const = 0;

	virtual void getFrame(std::vector<boost::uint16_t> &depthData, std::vector<boost::uint8_t> &playerData) = 0;

	virtual void nextFrame() = 0;
};