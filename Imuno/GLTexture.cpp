#include "GLTexture.h"
#include "ImunoException.h"
#include "Log.h"

using boost::format;
using std::string;

static Log logger;

GLTexture::GLTexture()
{
	frame = 0;
}


GLTexture::~GLTexture()
{
	if (!textureId.empty()) {
		glDeleteTextures(textureId.size(), textureId.data());
	}
	if (!surface.empty()) {
		for (int i = 0; i < surface.size(); i++) {
			SDL_FreeSurface(surface[i]);
		}
	}
}

ColorMode GLTexture::parseColorMode(const std::string &s)
{
	if (s == "modulate")
		return ColorModeModulate;
	else if (s == "add")
		return ColorModeAdd;
	else if (s == "subtract")
		return ColorModeSubtract;
	else
		throw ImunoException(format("Unknown colorMode %1%") % s);
}

std::string GLTexture::colorModeToString(ColorMode colorMode)
{
	switch (colorMode) {
	case ColorModeModulate:
		return "modulate";
	case ColorModeAdd:
		return "add";
	case ColorModeSubtract:
		return "subtract";
	default:
		return "unknown";
	}
}

void GLTexture::load(const std::string& file)
{
	if (file.find("%") != string::npos) {
		int i = 0;
		SDL_Surface *oneSurface = 0;
		do {
			string oneFile = (format(file) % i).str();
			oneSurface = IMG_Load(oneFile.c_str());
			if (oneSurface) {
				surface.push_back(oneSurface);
				logger.debug(format("seq-image#%1%: %2%") % i % oneFile);
				i++;
			}
		}
		while(oneSurface);

	}
	else {
		SDL_Surface *oneSurface = IMG_Load(file.c_str());
		if (oneSurface) {
			surface.push_back(oneSurface);
			logger.debug(format("single-image: %1%") % file);
		}
	}
	if (surface.empty()) {
		throw ImunoException("No images loaded");
	}

}

void GLTexture::setup()
{
	for (int i = 0; i < surface.size(); i++) {
		// get the number of channels in the SDL surface
		GLint nOfColors = surface[i]->format->BytesPerPixel;
		GLenum texture_format;
		if (nOfColors == 4)  { // contains an alpha channel
			texture_format = GL_RGBA;
		}
		else if (nOfColors == 3) { // no alpha channel
			texture_format = GL_RGB;
		}
 
		// Have OpenGL generate a texture object handle for us
		GLuint id;
		glGenTextures(1, &id);
		textureId.push_back(id);
		width.push_back(surface[i]->w);
		height.push_back(surface[i]->h);
 
		// Bind the texture object
		glBindTexture(GL_TEXTURE_2D, textureId[i]);
 
		// Edit the texture object's image data using the information SDL_Surface gives us
		glTexImage2D(GL_TEXTURE_2D, 0, nOfColors, surface[i]->w, surface[i]->h, 0,
					 texture_format, GL_UNSIGNED_BYTE, surface[i]->pixels);

		SDL_FreeSurface(surface[i]);
	}
	surface.clear();
	nFrames = textureId.size();

}

void GLTexture::draw(int x1, int y1, int x2, int y2, float r, float g, float b, float a, const ColorMode colorMode)
{
	switch (colorMode) {
	case ColorModeModulate:
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		break;
	case ColorModeAdd:
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD);
		break;
	case ColorModeSubtract:
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_SUBTRACT);
		break;
	}

	glColor4f(r, g, b, a);

	glBindTexture(GL_TEXTURE_2D, textureId[frame]);

	// Set the texture's stretching properties
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
 
	glBegin(GL_QUADS);

	// Bottom-left vertex (corner)
	glTexCoord2i(0, 0);
	glVertex2i(x1, y1);
 
	// Bottom-right vertex (corner)
	glTexCoord2i(1, 0);
	glVertex2i(x2, y1);
 
	// Top-right vertex (corner)
	glTexCoord2i(1, 1);
	glVertex2i(x2, y2);
 
	// Top-left vertex (corner)
	glTexCoord2i(0, 1);
	glVertex2i(x1, y2);

	glEnd();
}

void GLTexture::nextFrame() {
	frame ++;
	frame %= nFrames;
}

