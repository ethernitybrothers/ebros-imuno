#include "BodyPartAnimationRotate.h"
#include "Defaults.h"

#include <SDL.h>

BodyPartAnimationRotate::BodyPartAnimationRotate(void)
{
	rotationSpeed = Defaults::getRotationSpeed();
}


BodyPartAnimationRotate::~BodyPartAnimationRotate(void)
{
}

void BodyPartAnimationRotate::configure(const libconfig::Setting &setting)
{
	if (setting.exists("rotationSpeed")) {
		rotationSpeed = setting["rotationSpeed"];
	}
}

void BodyPartAnimationRotate::animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data)
{
	data.rotation += rotationSpeed * 360 * data.ticks / 60000.0;
}

