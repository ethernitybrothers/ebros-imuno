#pragma once
#include "BodyPartAnimation.h"

class BodyPartAnimationRotateByCoords : public BodyPartAnimation
{
public:
	BodyPartAnimationRotateByCoords(void);
	~BodyPartAnimationRotateByCoords(void);

	void animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data);

	void configure(const libconfig::Setting &setting);

private:
	float xFactor;
	float yFactor;
};

