#pragma once

#include "FloatFunction.h"

class LinearFunction : public FloatFunction
{
public:
	float operator() (float argument) const {
		return factor * (argument + offset_x) + offset_y;
	}

	LinearFunction() {
		offset_x = 0.0;
		offset_y = 0.0;
		factor = 1.0;
	}

	float offset_x;
	float offset_y;
	float factor;
};

