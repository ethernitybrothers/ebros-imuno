#pragma once

#include <stdexcept>
#include <boost/format.hpp>

class ImunoException : public std::runtime_error
{
public:
	ImunoException(boost::format &fmt) : std::runtime_error(fmt.str()) { }
	ImunoException(const std::string& message) : std::runtime_error(message) { }
	ImunoException(const char * message) : std::runtime_error(message) { }
	ImunoException(const ImunoException &e) : std::runtime_error(e) { }
};