#include "PlayerAnalyzer.h"
#include "Engine.h"
#include "PlayerData.h"
#include "PlayerSource.h"
#include "Log.h"

using boost::format;

static Log logger;


PlayerAnalyzer::PlayerAnalyzer(void)
{
}


PlayerAnalyzer::~PlayerAnalyzer(void)
{
}

void PlayerAnalyzer::configure(const libconfig::Setting &setting)
{
}

void PlayerAnalyzer::init(Engine &engine)
{
}

void PlayerAnalyzer::analyze(SceneData &sceneData)
{	
	const boost::uint8_t *frame = sceneData.playerIndexBuffer.data();
		
	int width = sceneData.width;
	int height = sceneData.height;
	sceneData.allocatePlayers(sceneData.maxPlayers);

	// players are numbered from 1..n
	for (int i = 1; i <= sceneData.maxPlayers; i++) {
		sceneData[i].topmost_y = height;
		sceneData[i].lowermost_y = 0;
		sceneData[i].leftmost_x = width;
		sceneData[i].rightmost_x = 0;
		sceneData[i].mass = 0;
	}
	const boost::uint8_t *walk;
	for (int y = 0; y < height; y++) {
		walk = frame + y*width;
		for (int x = 0; x < width; x++) {
			boost::uint8_t plr = walk[x];
			if (plr) {
				PlayerData & player = sceneData[plr];
				player.mass ++;
				if (x < player.leftmost_x) {
					player.leftmost_x = x;
					player.leftmost_y1 = y;
					player.leftmost_y2 = y;
				}
				else if (x == player.leftmost_x) {
					if (y < player.leftmost_y1)
						player.leftmost_y1 = y;
					if (y > player.leftmost_y2)
						player.leftmost_y2 = y;
				}

				if (x > player.rightmost_x) {
					player.rightmost_x = x;
					player.rightmost_y1 = y;
					player.rightmost_y2 = y;
				}
				else if (x == player.rightmost_x) {
					if (y < player.rightmost_y1)
						player.rightmost_y1 = y;
					if (y > player.rightmost_y2)
						player.rightmost_y2 = y;
				}

				if (y < player.topmost_y) {
					player.topmost_y = y;
					player.topmost_x1 = x;
					player.topmost_x2 = x;
				}
				else if (y == player.topmost_y) {
					if (x < player.topmost_x1)
						player.topmost_x1 = x;
					if (x > player.topmost_x2)
						player.topmost_x2 = x;
				}

				if (y > player.lowermost_y) {
					player.lowermost_y = y;
					player.lowermost_x1 = x;
					player.lowermost_x2 = x;
				}
				else if (y == player.lowermost_y) {
					if (x < player.lowermost_x1)
						player.lowermost_x1 = x;
					if (x > player.lowermost_x2)
						player.lowermost_x2 = x;
				}
			}
		}
	}
	for (int i = 1; i <= sceneData.maxPlayers; i++) {
		PlayerData &plr = sceneData[i];
		plr.leftmost_y1 = height - 1 - plr.leftmost_y1;
		plr.leftmost_y2 = height - 1 - plr.leftmost_y2;
		plr.rightmost_y1 = height - 1 - plr.rightmost_y1;
		plr.rightmost_y2 = height - 1 - plr.rightmost_y2;
		plr.topmost_y = height - 1 - plr.topmost_y;
		plr.lowermost_y = height - 1 - plr.lowermost_y;
		plr.topmost_x = (plr.topmost_x1 + plr.topmost_x2) / 2;
		plr.lowermost_x = (plr.lowermost_x1 + plr.lowermost_x2) / 2;
		plr.leftmost_y = (plr.leftmost_y1 + plr.leftmost_y2) / 2;
		plr.rightmost_y = (plr.rightmost_y1 + plr.rightmost_y2) / 2;
		plr.center_x = (plr.topmost_x + plr.leftmost_x + plr.rightmost_x + plr.lowermost_x) / 4;
		plr.center_y = (plr.topmost_y + plr.leftmost_y + plr.rightmost_y + plr.lowermost_y) / 4;
		/*
		if (i == 1) {
			logger.debug(format("Player %d data") % i);
			logger.debug(format("   topmost: %d,%d x1-2:%d-%d") % playerData[i].topmost_x % playerData[i].topmost_y % playerData[i].topmost_x1 % playerData[i].topmost_x2);
			logger.debug(format(" lowermost: %d,%d x1-2:%d-%d") % playerData[i].lowermost_x % playerData[i].lowermost_y % playerData[i].lowermost_x1 % playerData[i].lowermost_x2);
			logger.debug(format("  leftmost: %d,%d y1-2:%d-%d") % playerData[i].leftmost_x % playerData[i].leftmost_y % playerData[i].leftmost_y1 % playerData[i].leftmost_y2);
			logger.debug(format(" rightmost: %d,%d y1-2:%d-%d") % playerData[i].rightmost_x % playerData[i].rightmost_y % playerData[i].rightmost_y1 % playerData[i].rightmost_y2);
		}
		*/
	}
}
