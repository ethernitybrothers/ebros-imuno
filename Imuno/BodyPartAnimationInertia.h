#pragma once
#include "BodyPartAnimation.h"

#include <deque>

class FloatFunction;

class BodyPartAnimationInertia : public BodyPartAnimation
{
public:
	BodyPartAnimationInertia(void);
	~BodyPartAnimationInertia(void);

	void animate(Engine &engine, const PlayerData& playerData, BodyPartAnimationData& data);

	void configure(const libconfig::Setting &setting);

private:
	float strength;
	unsigned maxHistory;
	float strengthDamping;
	std::auto_ptr<FloatFunction> function;

	struct coord_t {
		int x, y;
		coord_t(int _x, int _y) : x(_x), y(_y) { }
		coord_t(const coord_t& copy) : x(copy.x), y(copy.y) { }
	};
	std::deque<coord_t> coord_history;
};
