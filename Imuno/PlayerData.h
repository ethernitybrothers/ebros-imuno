#pragma once

#include <vector>
#include <boost/cstdint.hpp>

#include "BodyPart.h"

enum PlayerPoint {
	PlayerPointTop,
	PlayerPointLeft,
	PlayerPointRight,
	PlayerPointBottom,
	PlayerPointCenter,
	PlayerPointBottomLeft,
	PlayerPointBottomRight,
	PlayerPointTopLeft,
	PlayerPointTopRight
};

class PlayerData {
public:
	int topmost_x1;
	int topmost_x2;
	int topmost_x;
	int topmost_y;
	int leftmost_x;
	int leftmost_y1;
	int leftmost_y2;
	int leftmost_y;
	int rightmost_x;
	int rightmost_y1;
	int rightmost_y2;
	int rightmost_y;
	int lowermost_x1;
	int lowermost_x2;
	int lowermost_x;
	int lowermost_y;
	int center_x;
	int center_y;
	long mass;

	bool active() const { return mass > 0; }

	void getPlayerPoint(PlayerPoint playerPoint, int &x, int &y) const;
	void getPlayerPoint(PlayerPoint playerPoint, float &x, float &y) const {
		int ix, iy;
		getPlayerPoint(playerPoint, ix, iy);
		x = (float) ix;
		y = (float) iy;
	}
};

class SceneData
{
public:
	SceneData(void);
	~SceneData(void);

	void clear();

	void allocatePlayers(int nPlayers);

	int width, height;
	int maxPlayers;
	std::vector<boost::uint16_t> depthBuffer;
	std::vector<boost::uint8_t> playerIndexBuffer;


	PlayerData & operator [] (int index) { return players[index]; }

	const PlayerData & operator [] (int index) const { return players[index]; }

private:
	std::vector<PlayerData> players;
};

