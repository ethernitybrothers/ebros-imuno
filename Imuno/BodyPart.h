#pragma once

#include <libconfig.hh>
#include <list>
#include <string>
#include <map>

#include "Layer.h"

class Engine;
class Player;
class BodyPartLayer;

enum PlayerPoint;

enum BodyPartType {
	BodyPartTop,
	BodyPartLeft,
	BodyPartRight,
	BodyPartBottom,
	BodyPartCenter,
	BodyPartFrame,
	BodyPartLine
};

class BodyPart : public Layer
{
public:
	BodyPart(Player *player);
	~BodyPart(void);

	void configure(const libconfig::Setting& setting);

	void init(Engine &engine);

	void preRender(Engine &engine);

	void render(Engine &engine);

	void postRender(Engine &engine);

	Player * getPlayer() {
		return player;
	}

	void getOffset( int &x1, int &y1, int &x2, int &y2 ) const {
		x1 = offsetX1;
		y1 = offsetY1;
		x2 = offsetX2;
		y2 = offsetY2;
	}

	BodyPartType getType() const {
		return type;
	}

	void getLinePoints(PlayerPoint &lineFrom, PlayerPoint &lineTo) const {
		lineFrom = this->lineFrom;
		lineTo = this->lineTo;
	}

private:
	Player *player;
	BodyPartType type;
	std::list<BodyPartLayer*> layers;
	bool enabled;
	int offsetX1;
	int offsetY1;
	int offsetX2;
	int offsetY2;
	PlayerPoint lineFrom;
	PlayerPoint lineTo;

	static std::map<std::string, BodyPartType> bodyPartTypeMap;
	static std::map<std::string, PlayerPoint> playerPointMap;
};
