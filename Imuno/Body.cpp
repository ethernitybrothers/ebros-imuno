#include "Body.h"
#include "Log.h"
#include "Engine.h"
#include "Defaults.h"
#include "Player.h"
#include "PlayerSource.h"
#include "PlayerData.h"
#include "Defaults.h"

using libconfig::Setting;
using boost::format;
using boost::uint32_t;
using boost::uint8_t;

static Log logger;

Body::Body(Player *player)
{
	this->player = player;
	this->stencilBuffer = 0;
	this->xindexes = 0;
	this->opacity = Defaults::getOpacity();
	this->zIndex = Defaults::getZindex();
}


Body::~Body(void)
{
	if (stencilBuffer)
		delete[] stencilBuffer;
	if (xindexes)
		delete[] xindexes;
}

void Body::configure(const libconfig::Setting &setting)
{
	Log::indent block(logger);
	const Setting &setting_img = setting["img"];
	std::string filename = (const char *) setting_img;
	logger.debug(format("file: %1%") % filename);
	texture.load(filename);
	if (setting.exists("z-index")) {
		zIndex = (int) setting["z-index"];
	}
	if (setting.exists("opacity")) {
		opacity = (float) setting["opacity"];
	}
	logger.debug(format("z-index: %1%") % zIndex);
	logger.debug(format("opacity: %1%") % opacity);
}

void Body::init(Engine &engine)
{
	int width, height;
	engine.getResolution(width, height);
	stencilBuffer = new boost::uint8_t[width*height];
	xindexes = new unsigned[width];
	texture.setup();
}

void Body::preRender(Engine &engine)
{
	engine.queue(this);
}

void Body::render(Engine &engine)
{
	int playerIndex = player->getIndex();
	SceneData &sceneData = engine.getSceneData();
	const boost::uint8_t *playerFrame = sceneData.playerIndexBuffer.data();
	int pw, ph, width, height;
	pw = sceneData.width;
	ph = sceneData.height;
	engine.getResolution(width, height);

	PlayerData &data = sceneData[playerIndex];

	// optimalization if resolution of both fits
	unsigned size = width * height;

	for (int x = 0; x < width; x++) {
		xindexes[x] = (x * pw) / width;
	}
	for (int y = 0; y < height; y++) {
		const boost::uint8_t *playerRow = playerFrame + ((y * ph) / height) * pw;
		for (int x = 0; x < width; x++) {
			stencilBuffer[x + y*width] = (playerRow[xindexes[x]] == playerIndex ? 128 : 0);
		}
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, width);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
	glDrawPixels(width, height, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, stencilBuffer);
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_LESS, 100, 255);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	int srcx1 = data.leftmost_x;
	int srcy1 = data.lowermost_y;
	int srcx2 = data.rightmost_x;
	int srcy2 = data.topmost_y;

	texture.draw(srcx1, srcy1, srcx2, srcy2, 1.0f, 1.0f, 1.0f, opacity, ColorModeModulate);

	glDisable(GL_STENCIL_TEST);
}

void Body::postRender(Engine &engine)
{
	texture.nextFrame();
}

